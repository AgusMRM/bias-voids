import numpy as np
import grispy as gsp
import healpy as hp 


def profile(x,y,z,r,grid,bines,bineado,particles = None ):
    """
    Calcula el perfil de partículas dentro de un conjunto de cascarones esféricos.

    Parámetros:
    x : array_like
        Coordenada x del centro.
    y : array_like
        Coordenada y del centro.
    z : array_like
        Coordenada z del centro.
    r : float
        Radio del cascarón esférico máximo para calcular el perfil.
    grid : objeto Grid
        Estructura de datos de tipo Grid que contiene la información de las partículas.
    bines : int
        Número de bines a lo largo del radio para dividir el cascarón esférico.
    bineado : array_like
        Lista de bordes de los bines para dividir el cascarón esférico.

    Retorna:
    particles : ndarray
        Array con la cantidad de partículas dentro de cada cascarón esférico definido por los bines.

    Notas:
        - Utiliza la el paquete GriSPy
    """
    if particles is None:
        particles = np.zeros(bines-1)
    
    for i in np.arange(bines-1):
        rmin = bineado[i]*r
        rmax = bineado[i+1]*r
        
        shell_dist, shell_ind = grid.shell_neighbors(
            np.array([[x,y,z]]),
            distance_lower_bound=rmin,
            distance_upper_bound=rmax
        )
        
        npar = len(shell_dist[0])
        particles[i] = npar
        
    return particles

def profile_volume(r,bines,bineado):
    
    profile = np.zeros(bines-1)
    cte = (4.0/3.0)*np.pi
    
    for i in np.arange(bines-1):
        rmin = bineado[i]*r
        rmax = bineado[i+1]*r
        
        vol = cte*(rmax**3-rmin**3)
        profile[i] = vol
        
    return profile

def periodicidad_old(nbin,bines):
    
    if (nbin==bines):
            bin_out = 0
    #if (nbin==bines + 1): 
    #        bin_out = 1
    elif (nbin<0):
            bin_out = bines + nbin
    elif (nbin>bines):
            bin_out = nbin - (bines + 1)
    else :
            bin_out = nbin
    
    return bin_out



def generar_bines_old(binx, biny, binz, nbines, ngrid):
    
    bines_alrededor = []
    
    for i in range(binx - nbines, binx + nbines + 1):
        
        ibin = periodicidad(i,ngrid)
        
        for j in range(biny - nbines, biny + nbines + 1):
        
            jbin = periodicidad(j,ngrid)

            for k in range(binz - nbines, binz + nbines + 1):
        
                kbin = periodicidad(k,ngrid)

                bines_alrededor.append((ibin, jbin,kbin))
    
    return bines_alrededor

def distance_correction_periodicity(distance,lbox):
    if (distance > lbox/2):
        distance = abs(distance-lbox)
        
    return distance

def profile_bias(x,y,z,rvoid,field,bines,bineado,lbox,profile = None, particles = None):
    
    if profile == None:
        profile = np.zeros(bines) #el primer indice del perfil va a llevar el radio del void
        particles = np.zeros(bines)
    

    # tomo el rango de distancias del perfil (en unidades de rvoid)
    rmin = bineado[0]
    maxbin = bineado.shape[0] - 1
    rmax = bineado[maxbin]
    #abin = (np.log10(rmax)-np.log10(rmin))/bines
    abin = (rmax-rmin)/bines
    
    # fbines es el numero de bines del field (ej: 540). 
    # Calculamos entonces el tamaño de cada uno de los bines
    fbines = field.shape[0]
    bin_size = lbox/fbines
    
    # cuantas capas de bines tengo que recorrer.
    nbines = int(rvoid*rmax/bin_size)
    #print(rvoid,rmax,bin_size,nbines)
    # bines (en el field) donde esta el centro del void
    binx0 = int(x/bin_size)
    biny0 = int(y/bin_size)
    binz0 = int(z/bin_size)
    
    seleccion_bines = generar_bines(binx0, biny0, binz0, nbines,fbines)
    bin_number = len(seleccion_bines)
    
    for i in range(bin_number):
        
        binx = seleccion_bines[i][0]
        biny = seleccion_bines[i][1]
        binz = seleccion_bines[i][2]
    
        posx = binx*bin_size + bin_size/2
        posy = biny*bin_size + bin_size/2
        posz = binz*bin_size + bin_size/2
        
        bias_value = field[binx,biny,binz]
        
        dx = abs(posx-x)
        dx = distance_correction_periodicity(dx,lbox)
        dy = abs(posy-y)
        dy = distance_correction_periodicity(dy,lbox)
        dz = abs(posz-z)
        dz = distance_correction_periodicity(dz,lbox)
        
        d = np.sqrt(dx**2 + dy**2 + dz**2)
        d_normalize = d/rvoid
        #nbin = int(np.log10(d_normalize)/abin)
        nbin = int(d_normalize/abin)
        #print(d,d_normalize,nbin,bines)
        
        # al tomar un cuadrado alrededor del centro, 
        # las esquinas del cuadrado se me van del radio buscado. 
        if d_normalize < rmax:
            profile[nbin] = profile[nbin] + bias_value
            particles[nbin] = particles[nbin] + 1
     
    return profile, particles 

def mean_profile(perfiles):
    
    nbines = perfiles.shape[0]
    perfil_medio = np.zeros(nbines)
    
    for i in range(nbines):
            mask = ~np.isnan(perfiles[i,:])
            perfil_medio[i] = np.mean(perfiles[i,mask])
            
    return perfil_medio

def median_profile(perfiles,q1,q2):
    
    nbines = perfiles.shape[0]
    perfil_median = np.zeros(nbines)
    perfil_q1 = np.zeros(nbines)
    perfil_q2 = np.zeros(nbines)
    
    for i in range(nbines):
            mask = ~np.isnan(perfiles[i,:])
            median = np.median(perfiles[i,mask])
            perfil_median[i] = median
            
            percentil1 = np.percentile(perfiles[i,mask], q1)
            percentil2 = np.percentile(perfiles[i,mask], q2)
            perfil_q1[i] = percentil1
            perfil_q2[i] = percentil2
            
    return perfil_median, perfil_q1, perfil_q2

def helpy_points(x0,y0,z0,r):
    
    nside = 2
    theta, phi = hp.pix2ang(nside, np.arange(12 * nside ** 2))
    x = r*np.sin(phi)*np.cos(theta) - x0
    y = r*np.sin(phi)*np.sin(theta) - y0
    z = r*np.cos(phi) - z0
    
    return x,y,z

def ejes(ax):
    
    ax.tick_params('both',length=5,width=1.2,which='minor',direction='in',right='on',top='on')
    ax.tick_params('both',length=8,width=1.2,which='major',direction='in',right='on',top='on')
    ax.tick_params(labelsize=15)
    ax.minorticks_on()
    
def fibo_points(x0,y0,z0,d,nsamples,ngrid):
        
    x = []
    y = []
    z = []
    
    
    offset = 2.0 / (nsamples);                                                                                                                                                
    increment = np.pi * (3.0 - np.sqrt(5.0));                                                                                                                                              
    
    for i in range(nsamples):
        yt = ((i * offset) - 1) + (offset / 2);                                                                                                                                             
        r = np.sqrt(1 - yt * yt)*d;                                                                                                                                                          
        phi = ((i + 1) % nsamples) * increment;                                                                                                                                        
        x.append(np.cos(phi) * r )                                                                                                                                                                 
        y.append(yt)
        z.append(np.sin(phi) * r)
        
    return np.array(x)+x0,np.array(y)+y0,np.array(z)+z0

def helpix_points(x0,y0,z0,r,ngrid):

    #ngrid = 20
    nside = 8
    #theta, phi = hp.pix2ang(nside, np.arange(12 * nside ** 2))
    p = np.linspace(0,1-1/ngrid,ngrid)
    phi = 2*np.pi*p
    theta = np.arccos(2*np.linspace(0,1,ngrid + 1) - 1) 
    for i in phi:
        for j in theta:
            
            x.append(r*np.sin(i)*np.cos(j) - x0)
            y.append(r*np.sin(i)*np.sin(j) - y0)
            z.append(r*np.cos(i) - z0)
    
    return np.array(x),np.array(y),np.array(z)

def profile_bias_fibo(x,y,z,rvoid,field,bines,bineado,lbox,profile = None, particles = None):
    
    if profile == None:
        profile = np.zeros(bines )
        particles = np.zeros(bines )
        
    # tomo el rango de distancias del perfil (en unidades de rvoid)
    rmin = bineado[0]
    maxbin = bineado.shape[0] - 1
    rmax = bineado[maxbin]
    #abin = (np.log10(rmax)-np.log10(rmin))/bines
    abin = (rmax-rmin)/bines
    
    # fbines es el numero de bines del field (ej: 540). 
    # Calculamos entonces el tamaño de cada uno de los bines
    fbines = field.shape[0]
    bin_size = lbox/fbines
    
    # cuantas capas de bines tengo que recorrer.
    nbines = int(rvoid*rmax/bin_size)
    #print(rvoid,rmax,bin_size,nbines)
    # bines (en el field) donde esta el centro del void
    binx0 = int(x/bin_size)
    biny0 = int(y/bin_size)
    binz0 = int(z/bin_size)
    
    seleccion_bines = generar_bines(binx0, biny0, binz0, nbines,fbines)
    bin_number = len(seleccion_bines)
    
    for i in range(bin_number):
        
        binx = seleccion_bines[i][0]
        biny = seleccion_bines[i][1]
        binz = seleccion_bines[i][2]
    
        posx = binx*bin_size + bin_size/2
        posy = biny*bin_size + bin_size/2
        posz = binz*bin_size + bin_size/2
        
        bias_value = field[binx,biny,binz]
        
        dx = abs(posx-x)
        dx = distance_correction_periodicity(dx,lbox)
        dy = abs(posy-y)
        dy = distance_correction_periodicity(dy,lbox)
        dz = abs(posz-z)
        dz = distance_correction_periodicity(dz,lbox)
        
        d = np.sqrt(dx**2 + dy**2 + dz**2)
        d_normalize = d/rvoid
        #nbin = int(np.log10(d_normalize)/abin)
        nbin = int(d_normalize/abin)
        #print(d,d_normalize,nbin,bines)
        
        # al tomar un cuadrado alrededor del centro, 
        # las esquinas del cuadrado se me van del radio buscado. 
        if d_normalize < rmax:
            profile[nbin] = profile[nbin] + bias_value
            particles[nbin] = particles[nbin] + 1
        
    return profile, particles 

def generate_circle_matrix_3d(n, r, dim):
    # Crear una matriz tridimensional de ceros de tamaño n x n x n
    matrix = np.zeros((n,) * dim, dtype=np.float32)

    # Calcular el centro de la matriz
    center = (n - 1) / 2

    # Crear una cuadrícula de coordenadas
    coordinates = np.meshgrid(*[np.arange(n) for _ in range(dim)], indexing='ij')
    # Calcular la distancia desde cada punto al centro
    distance = np.sqrt(np.sum((coord - center)**2 for coord in coordinates))

    # Asignar 1 a los puntos dentro del círculo central
    matrix[distance <= r] = 1

    return matrix


def asignacion_pesos(resto):
    
    if resto <= 0.5:
        w0 = 1 - resto
        w1 = resto
    if resto > 0.5:
        w0 = 1 - resto
        w1 = resto

    return w0,w1

def interpolador(x,y,z,field,bin_size):
    
    fbines = field.shape[0]
    
    # dado un punto en el espacio, calculo "en que bin esta"
    binx = int(x/bin_size)
    biny = int(y/bin_size)
    binz = int(z/bin_size)

    # aplicar periodicidad.

    restox = x%1
    restoy = y%1
    restoz = z%1

    vol_tot = bin_size**3
    
    px0, px1 = asignacion_pesos(restox) 
    py0, py1 = asignacion_pesos(restoy) 
    pz0, pz1 = asignacion_pesos(restoz) 
    
    px0 = px0 * bin_size; px1 = px1 * bin_size
    py0 = py0 * bin_size; py1 = py1 * bin_size
    pz0 = pz0 * bin_size; pz1 = pz1 * bin_size
    # en python, se redondea al entero mas bajo, por lo tanto, 
    # para recorrer los 8 "puntos" o (bines adyacendes) voy 
    # a interpolar con los punts binx+1, biny+1, binz+1
    
    binx0 = periodicidad(binx,fbines)
    binx1 = periodicidad(binx + 1,fbines)
    biny0 = periodicidad(biny,fbines)
    biny1 = periodicidad(biny + 1,fbines)
    binz0 = periodicidad(binz,fbines)
    binz1 = periodicidad(binz + 1,fbines)
        
    interpolated_value = (  field[binx0,biny0,binz0] * px0 * py0 * pz0 / vol_tot
                     + field[binx1,biny0,binz0] * px1 * py0 * pz0 / vol_tot
                     + field[binx1,biny1,binz0] * px1 * py1 * pz0 / vol_tot
                     + field[binx0,biny1,binz0] * px0 * py1 * pz0 / vol_tot
                     + field[binx0,biny0,binz1] * px0 * py0 * pz1 / vol_tot
                     + field[binx1,biny0,binz1] * px1 * py0 * pz1 / vol_tot
                     + field[binx1,biny1,binz1] * px1 * py1 * pz1 / vol_tot
                     + field[binx0,biny1,binz1] * px0 * py1 * pz1 / vol_tot
                    )
    
    
    return interpolated_value

def celda_random(bin_size, num_puntos):
    """
    Asigna puntos aleatorios dentro de un box tridimensional de tamaño bin_size de lado.
    
    Parámetros:
    bin_size (float): Tamaño del lado del box.
    num_puntos (int): Número de puntos a generar.
    
    Retorna:
    np.ndarray: Array de forma (num_puntos, 3) con las coordenadas de los puntos.
    """
    
    # Generar puntos aleatorios dentro del rango [0, bin_size) para cada dimensión
    puntos = np.random.uniform(low=0, high=bin_size, size=(num_puntos, 3))
    
    return puntos    


def weight_sphere(fbines,bin_size,R,npoints):
    
    points = celda_random(bin_size,npoints)
    sphere = np.zeros([fbines,fbines,fbines],dtype=np.float32)

    x0 = (fbines/2)*bin_size - bin_size/2  # centro la esfera en el medio del bin
    y0 = (fbines/2)*bin_size - bin_size/2  # centro la esfera en el medio del bin
    z0 = (fbines/2)*bin_size - bin_size/2  # centro la esfera en el medio del bin

    binx0 = int(x0/bin_size)
    biny0 = int(x0/bin_size)
    binz0 = int(x0/bin_size)

    # centro de la esfera
    center = np.array([x0,y0,z0])

    # que bines recorrer:
    bines_alrededor = generar_bines(binx0, biny0, binz0, int(R+2), fbines)
    
    Rscale = R*bin_size

    for bines in bines_alrededor:
    
        # bines de la celda
        binx = bines[0]
        biny = bines[1]
        binz = bines[2]
        
        # posicion de la celda
        posx = binx*bin_size
        posy = biny*bin_size
        posz = binz*bin_size
        
        # CENTRAR CELDA RANDOM ACA:
        random = np.zeros([npoints,3],dtype=np.float32)
        random[:,0] = posx + points[:,0]
        random[:,1] = posy + points[:,1]
        random[:,2] = posz + points[:,2]
        
        # calculo distancias de los puntos a el centro de la esfera
        dx = random[:,0] - x0
        dy = random[:,1] - y0
        dz = random[:,2] - z0
        distances = np.sqrt(dx**2 + dy**2 + dz**2)
        mask, = np.where(distances < Rscale)    
        
        # asigno peso
        w = len(mask)/npoints
        sphere[binx,biny,binz] = w
        
    return sphere

def generar_lista(rmin, rmax,njump):
    # Lista que va de 1 a rmin incrementando de a 1
    primera_parte = list(range(1, rmin ))
    
    # Lista que va de rmin a rmax incrementando de a 0.25
    segunda_parte = [rmin + njump * i for i in range(0, int((rmax - rmin) / njump) + 1)]
    
    # Combinar ambas listas
    lista_completa = primera_parte + segunda_parte
    
    return np.array(lista_completa).astype(np.float32)

def periodicidad(nbin, bines):
    return (nbin % (bines) + (bines)) % (bines)

def generar_bines(binx, biny, binz, nbines, ngrid):
    rango_i = range(binx - nbines, binx + nbines + 1)
    rango_j = range(biny - nbines, biny + nbines + 1)
    rango_k = range(binz - nbines, binz + nbines + 1)
    
    bines_alrededor = [
        (periodicidad(i, ngrid), periodicidad(j, ngrid), periodicidad(k, ngrid))
        for i in rango_i for j in rango_j for k in rango_k
    ]
    
    return bines_alrededor

def perfil_jack(perfiles,realizaciones):
    voidnumber = perfiles.shape[0]
    voidbines = perfiles.shape[1]
    ndivision = int(voidnumber/realizaciones)

    perfiles_jack = np.zeros([realizaciones,voidbines])
    
    total_ids = np.arange(0,voidnumber)

    exclude0 = 0
    for i in range(realizaciones):
        exclude1 = exclude0 + ndivision
    
    # selecciono (excluyo) los indices para hacer el jacknife
        selection = total_ids[(total_ids < exclude0) | (total_ids > exclude1)]
        perfiles_jack[i,:] = mean_profile(perfiles[selection,:].T)
        
        exclude0 = exclude1
        
    perfiles_mean = np.zeros(voidbines)
    perfiles_error = np.zeros(voidbines)

    n = realizaciones
    for i in range(voidbines):
        media = np.mean(perfiles_jack[:,i])
        error = np.sqrt(((n-1)/n) * np.sum((perfiles_jack[:,i]-media)**2))
    
        perfiles_mean[i]  = media
        perfiles_error[i] = error
        
    return perfiles_mean, perfiles_error
