import pandas as pd
import programas as pg
from scipy.ndimage import convolve
from scipy.signal import fftconvolve
import numpy as np 
from joblib import Parallel, delayed
import gc
import sys

name = 'rhogal_eul_bias_expansion'
path = '/mnt/projects/bias_mock/'
field = np.load(path+name+'.npy')
nbines = 20
fbines = field.shape[0]
nthreads = nbines




def convolucion_campos(field,circle_radio):
    
    i = circle_radio
    print(i)
    campo_extendido = np.pad(field,  [(i, i)]*3, mode='wrap')
    print('campo extendido ',i)
    kernel_radio = i + 1
    kernel = pg.generate_circle_matrix_3d(540, kernel_radio, dim=3)
    # normalize the kernel
    kernel = kernel/np.sum(kernel)

    # Realizar la convolución
    campo_convolucionado = fftconvolve(campo_extendido, kernel, mode='same')
    
    center_field = campo_convolucionado[i:i+fbines,i:i+fbines,i:i+fbines]
    # guardar campo
    file = np.save(path+'convolution_'+name+str(i)+'.npy',center_field)
    
    

#Parallel(n_jobs=nthreads, require='sharedmem')(delayed(convolucion_campos)(field,i) for i in np.arange(nbines))                                                                             

for i in range(nbines):
    funcion = convolucion_campos(field,i)
#    gc.collect() 

#nprocesses = 20
#sys.stdout.write('reading chunks..')
#sys.stdout.flush()
#
## Se divide el trabajo en una lista de trabajos
## El indice de la primera lista va a ser el proceso que se va a encargar del trabajo
## Y la lista de ese indice los chunks que va a procesar.
#
#lists = [[] for _ in range(nprocesses)]
#for ind in range(chunks):
#    lists[ind % nprocesses].append(ind)
#
#
#processes = [Process(target=trees, args=(x,lists)) for x in range(nprocesses)]
## Comienza los procesos
#for p in processes:
#    p.start()
## se unen los procesos
#for p in processes:
#    p.join()
