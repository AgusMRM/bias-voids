import h5py 
import programas as pg
import pandas as pd
import grispy as gsp
import numpy as np

masa = '5e-04'
file = h5py.File('/mnt/projects/bias_mock/galaxies/galpos_RedSample_'+masa+'.hdf5','r')
pos = file['Subhalo/SubhaloPos'][()]
v0 = np.loadtxt('/mnt/projects/bias_mock/voids/sphvds_raul-RedSample_'+masa+'-0.9.dat')
centres = v0[:,2:5]

Lbox = 1440
grid = gsp.GriSPy(pos, N_cells=32)
periodic = {0: (0, Lbox), 1: (0, Lbox), 2: (0, Lbox)}
grid.set_periodicity(periodic, inplace=True)

bines = 30
rmin = 0
rmax = 5
bineado = np.linspace(rmin,rmax,bines)

npart = len(pos)
rhomean = npart/Lbox**3

nvoids = len(v0)

perfiles = np.zeros([bines-1,nvoids])
for i in range(nvoids):

    centres = v0[i,2:5]
    rvoid = v0[i,1]
    x = centres[0]
    y = centres[1]
    z = centres[2]

    perfil = pg.profile(x,y,z,rvoid,grid,bines,bineado)
    volumen = pg.profile_volume(rvoid,bines,bineado)
    
    delta = ((perfil/volumen)-rhomean)/rhomean
    
    perfiles[:,i] = delta
    
    
mean_galaxies = pg.mean_profile(perfiles)
data = {'distance': bineado[1:],
        'media': mean_galaxies,
        }

df = pd.DataFrame(data)
df.to_csv('/home/arodriguez/BACCO/bias_mock/perfiles/perfiles_prop_'+masa+'_gtrazers_-0.9.csv',index=False)