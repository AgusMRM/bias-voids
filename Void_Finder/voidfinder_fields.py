import pandas as pd
from scipy.ndimage import convolve
from scipy.signal import fftconvolve
import numpy as np 
from joblib import Parallel, delayed
from multiprocessing import Pool
import gc
import sys
import fields as fs
from concurrent.futures import ThreadPoolExecutor
from numba import njit, prange
import programas as pg 
import vfinder_mod as vf
import smoothing_library as SL
import h5py
import MAS_library as MASL
import grispy as gsp
import random
# transformo posiciones a grilla

masa = '5e-04'

#smooth = 20   # smoothed scale in Mpc/h
delta_threshold = -0.9
nbines = 50

# radios maximos y minimos en unidades de celdas
rmin = 7

path = '/mnt/projects/bias_mock/galaxies/'
filename = 'rhogalz_'+masa+'_eul_bias_gaus'
field = np.load(path+filename+'.npy')

lbox = 1440.0 #Mpc/h ; size of box
path_out = '/home/arodriguez/BACCO/bias-voids/Void_Finder/voids/real_space/'
voidfilename_out = 'voids_'+filename

fbines = field.shape[0]
bin_size = lbox/fbines

delta = field/np.mean(field) - 1    
print('min delta:',np.min(delta),np.min(field)) 
del field


void_field = np.zeros([fbines,fbines,fbines], dtype = np.float32)


##########################################################
radios_to_convolve = pg.generar_lista(rmin,rmin+nbines,0.25)

marker_first_iteration = 1
for radio in radios_to_convolve[rmin-1:]:

    print('convolution...', radio)
    
    R = radio*bin_size
    field_integrated = fs.convolution_modified(delta, radio, bin_size)
    print('done')
    
    sel = np.where(field_integrated < delta_threshold)
    
    try:
        print('sel0 len:',len(sel0[0]))
    except NameError:
        print('sel0 no existe')
    
    if (len(sel[0]) == 0):
        print('termino busqueda de void center candidates')
        break
        
    if marker_first_iteration == 0:
        # tengo que controlar que los sel hayan estado en sel0 
        new_sel = vf.check_growth(sel0,sel)
        if (type(new_sel)==tuple):
            void_field[new_sel] = radio
            sel0 = new_sel
        if (new_sel == -99):
            print('esferas compensadas con anterioridad')
        
    if marker_first_iteration == 1:
        print('primera iteracion', 'marker first iteration:', marker_first_iteration)
        void_field[sel] = radio
        marker_first_iteration = 0
        sel0 = sel
    del field_integrated

gc.collect()

print('radios de voids encontrados:',np.unique(void_field))

    
#print('saving void candidates field')
#np.save('/home/arodriguez/BACCO/bias-voids/void_finder/'+name+'_void_field.npy',void_field)

# esto lo estoy utilizando para testo, sino comentarearlo.
#void_field = np.load('/home/arodriguez/BACCO/bias-voids/void_finder/'+name+'_void_field.npy')

print('starting cleaning..')

positions_clean, radios_clean = vf.clean_inner_regions(void_field,lbox,rmin)
#
data = {'x':positions_clean[:,0],
        'y':positions_clean[:,1],
        'z':positions_clean[:,2],
        'r':radios_clean*bin_size
        }
## Guardar los datos en un archivo CSV
df = pd.DataFrame(data)
df.to_csv(path_out+voidfilename_out+'_no_clean_'+str(delta_threshold)+'_pruebajump_final.dat',index=False)

pos, rad = vf.segunda_limpieza(positions_clean, radios_clean,lbox,rmin,bin_size)
data = {'x':pos[0][:,0],
        'y':pos[0][:,1],
        'z':pos[0][:,2],
        'r':rad*bin_size
        }
df = pd.DataFrame(data)
df.to_csv(path_out+voidfilename_out+'_clean_'+str(delta_threshold)+'_pruebajump_final.dat',index=False)

print('program finished')
