import pandas as pd
import grispy as gsp
import healpy as hp 
import programas as pg
import numpy as np
from multiprocessing import Pool
import MAS_library as MASL
from icosphere import icosphere

np.seterr(invalid='ignore')


def calcular_perfiles(id_void):
    x = v[seleccion,0][id_void] #v[sel, 0][id_void]
    y = v[seleccion,1][id_void] #v[sel, 1][id_void]
    z = v[seleccion,2][id_void] #v[sel, 2][id_void]
    rvoid = v[seleccion,3][id_void] #v[sel, 3][id_void]
    return perfil_2d(x, y, z, bineado, rvoid, field, bin_size, lbox)

def icosphere_points(x0,y0,z0,r,nu):
        
    x = []
    y = []
    z = []
    
    vertices, faces = icosphere(nu)
    x = vertices[:,0]*r
    y = vertices[:,1]*r
    z = vertices[:,2]*r
        
    return np.array(x)+x0,np.array(y)+y0,np.array(z)+z0

def perfil_2d(x, y, z, bineado, rvoid, field, bin_size, lbox):
    
    ngrid = field.shape[0]
    bines = bineado.shape[0] - 1
    perfil = np.zeros([bines,bines], dtype=np.float32)  # Asegúrate de que el perfil sea float32
    counts = np.zeros([bines,bines], dtype=np.float32)
    abin = bineado[1] - bineado[0]
    binx_centro = int(bines/2)
    biny_centro = int(bines/2)
    
    for radio in bineado[1:]:
        
        radio_mpc = rvoid*radio - bin_size #resto bin_size para centrar
        
        nu = 20    
        posx, posy, posz = icosphere_points(x, y, z, radio_mpc, nu)
        puntos = len(posx)
        #field_count = []
        
        for j in range(puntos):
            px = posx[j]
            py = posy[j]
            pz = posz[j]
            
            # calculo distancias para sigma-pi
            dx = distance_with_sign(px,x,lbox)
            dy = distance_with_sign(py,y,lbox)
            dz = distance_with_sign(pz,z,lbox)
                    
            pi = dz
            sigma = np.sqrt(dx**2 + dy**2)
            # veo si sigma es positivo o negativo. 
            if dx > 0:
                sigma = sigma
            if dx <= 0:
                sigma = - sigma
            
            
            ### sigo con los puntos para la esfera            
            px = px % lbox
            py = py % lbox
            pz = pz % lbox        
        
            pos = np.array([[px, py, pz]], dtype=np.float32)  # Asegúrate de que pos sea float32
            den = np.zeros(pos.shape[0], dtype=np.float32)
            MASL.CIC_interp(field, lbox, pos, den)
            #field_count.append(den[0])
            
            
            
            binx = int((sigma/rvoid)/abin) + binx_centro
            biny = int((pi/rvoid)/abin) + biny_centro
            
            perfil[binx,biny] += den[0]
            counts[binx,biny] += 1
    
    return perfil/counts

def distance_with_sign(p,c, lbox):
    d = abs(p - c)
    if d > lbox/2:
        d = lbox - d

    if p%lbox > c:
        d = d
    if p%lbox < c:
        d = - d
    return d

smooth = 0
masa = '5e-04'
delta_thresold = -0.9
fbines = 540
space = 'redshift_space'
#field_name = 'rhodm_eul'
field_name = 'rhogalz_'+masa+'_eul_bias_gaus'
#field_name = 'galposz_RedSample_'+masa+'_smooth_'+str(smooth)+'_'+str(fbines)   

#field = np.load('/mnt/projects/bias_mock/galaxies/smoothed_fields/PL/'+field_name+'.npy').astype(np.float32)
field = np.load('/mnt/projects/bias_mock/galaxies/'+field_name+'.npy').astype(np.float32)
field = field/np.mean(field) - 1    # activar solo para fiels hechos por raul

path_out = '/home/arodriguez/BACCO/bias-voids/Void_Finder/voids/'+space+'/'+str(fbines)+'/'
#filename_out = 'perfiles_'+field_name+'_'+str(smooth)#+'_gaus_voids'

#field = np.load(field_name+'.npy').astype(np.float32)  # Convertir a float32
#v = np.loadtxt('/home/arodriguez/BACCO/bias-voids/Void_Finder/voids/'+space+'/'+str(fbines)+'/voids_'+masa+'_smooth_'+str(smooth)+'_clean_'+str(delta_thresold)+'_pruebabines.dat',skiprows=1,delimiter=',')
#v = np.loadtxt('/mnt/projects/bias_mock/voids/sphvds_raul-RedSample_'+masa+'_'+str(delta_thresold)+'.dat')
v = np.loadtxt('/home/arodriguez/BACCO/bias-voids/Void_Finder/voids/'+space+'/voids_rhogalz_5e-04_eul_bias_gaus_clean_-0.9_pruebabines.dat',skiprows=1,delimiter=',')

#sel, = np.where((v[:, 0] > 300) & (v[:, 0] < 1100) & (v[:, 1] > 300) & (v[:, 1] < 1100) & (v[:, 2] > 300) & (v[:, 2] < 1100))
print('fbines:',fbines,field.shape[0])
num_procesos = 120

nbines = 80
rmin = -4
rmax = 4
bineado = np.linspace(rmin, rmax, nbines + 1)
print(bineado)

max_rvoid = 100
min_rvoid = 30
seleccion, = np.where((v[:,3]>min_rvoid) & (v[:,3]<max_rvoid))
nvoids = len(seleccion)
print('cantidad de voids en rango:', nvoids)

rhomean = np.mean(field)
ngrid = field.shape[0]
lbox = 1440
bin_size = lbox / ngrid


if __name__ == "__main__":
    id_voids = range(nvoids)
    with Pool(processes=num_procesos) as pool:
        perfiles = pool.map(calcular_perfiles, id_voids)
    
    perfiles = np.array(perfiles, dtype=np.float32)
    print('perfiles shape:',perfiles.shape)
    
    
    np.save('correlation_gaus'+space+'_full.npy',perfiles)
    
    
    #median, q1, q2 = pg.median_profile(perfiles.T, 25, 75)
    #mean = pg.mean_profile(perfiles.T)
#
    #data = {'distance': bineado[1:],
    #        'media': mean,
    #        'mediana': median,
    #        'q25': q1,
    #        'q75': q2}
#
    #df = pd.DataFrame(data)
    #df.to_csv('perfiles_interp_' + field_name + '_pruebas.csv', index=False)
