import pandas as pd
from scipy.ndimage import convolve
from scipy.signal import fftconvolve
import numpy as np 
from joblib import Parallel, delayed
from multiprocessing import Pool
import gc
import sys
import fields as fs
from concurrent.futures import ThreadPoolExecutor
from numba import njit, prange
import programas as pg 
import vfinder_mod_new as vf
import smoothing_library as SL
import h5py
import MAS_library as MASL
import grispy as gsp
import random
import time
# transformo posiciones a grilla

masa = '2e-03'
fbines = 540 #the 3D field will have grid x grid x grid voxels
smooth = 0   # smoothed scale in Mpc/h
delta_threshold = -0.9
nbines = 70

# radios maximos y minimos en Mpc
rmin = 10
rmax = 40

path = '/mnt/projects/bias_mock/galaxies/smoothed_fields/PL/'
filename = 'galposz_RedSample_'+masa+'_'+str(smooth)
delta = np.load(path+filename+'.npy')

lbox = 1440.0 #Mpc/h ; size of box
path_out = '/home/arodriguez/BACCO/bias-voids/Void_Finder/voids/redshift_space/'+str(fbines)+'/'
voidfilename_out = 'voids_'+filename

bin_size = lbox/fbines
threads = 56


print('min delta:',np.min(delta)) 

void_field = np.zeros([fbines,fbines,fbines], dtype = np.float32)


##########################################################
#radios_to_convolve = pg.generar_lista(rmin,rmin+nbines,0.5)
radios_to_convolve = np.linspace(rmin,rmax,(rmax-rmin)+1)

marker_first_iteration = 1
contador_final = 0
for R in radios_to_convolve:
    
    #R = radio*bin_size
    radio = R/bin_size
    print('convolution...', R, radio)
    time_start = time.time()
    field_integrated = fs.convolution_modified(delta, radio, bin_size)
    print('done in:', time.time()-time_start)
    
    sel = np.where(field_integrated < delta_threshold)
    candidates = len(sel[0])
    print('candidates:',candidates)
    
    # si no es la primera iteracion, encontre radio de void posible
    if ((candidates > 0) & (marker_first_iteration == 0)):
        void_field[sel] = radio
        # me aseguro que el contador final sea 0 (en caso de haberlo inicializado en paso previo)
        contador_final = 0
        
    # en la primera iteracion que encuentre voids creo mi contador
    # para las ultimas iteraciones
    if ((marker_first_iteration == 1) & (candidates > 0)):
        void_field[sel] = radio
        contador_final = 0
        marker_first_iteration = 0        
        
    
    # si no encuentro nada, y no estoy en las primeras iteraciones, comienzo
    # conteo final
    if ((marker_first_iteration == 0) & (candidates == 0)):
        contador_final += 1
        print('contador final:', contador_final)
        
    # luego de 3 iteraciones seguidas sin encontrar voids, termino la busqueda.
    if contador_final == 2:
        break
    
    del field_integrated
    

gc.collect()

print('radios de voids encontrados:',np.unique(void_field))

    
#print('saving void candidates field')
#np.save('/home/arodriguez/BACCO/bias-voids/void_finder/'+name+'_void_field.npy',void_field)

# esto lo estoy utilizando para testo, sino comentarearlo.
#void_field = np.load('/home/arodriguez/BACCO/bias-voids/void_finder/'+name+'_void_field.npy')

print('starting cleaning..')

positions_clean, radios_clean = vf.clean_inner_regions(void_field,lbox,rmin)
#
data = {'x':positions_clean[:,0],
        'y':positions_clean[:,1],
        'z':positions_clean[:,2],
        'r':radios_clean*bin_size
        }
## Guardar los datos en un archivo CSV
df = pd.DataFrame(data)
#df.to_csv(path_out+voidfilename_out+'_no_clean_'+str(delta_threshold)+'.dat',index=False)

pos, rad = vf.segunda_limpieza(positions_clean, radios_clean,lbox,rmin,bin_size)
data = {'x':pos[0][:,0],
        'y':pos[0][:,1],
        'z':pos[0][:,2],
        'r':rad*bin_size
        }
df = pd.DataFrame(data)
#df.to_csv(path_out+voidfilename_out+'_clean_'+str(delta_threshold)+'.dat',index=False)
df.to_csv('voids.dat',index=False)

print('program finished')
