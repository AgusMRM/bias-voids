import pandas as pd
from scipy.ndimage import convolve
from scipy.signal import fftconvolve
import numpy as np 
from joblib import Parallel, delayed
from multiprocessing import Pool
import gc
import sys
import fields as fs
from concurrent.futures import ThreadPoolExecutor
from numba import njit, prange
import programas as pg
import grispy as gsp
import MAS_library as MASL
import random
import time
from numba import njit, prange



                
def initialize_grid(grid_shared):
    global grid
    grid = grid_shared
    
    return grid

def create_convolution(field, cell_radio):
    circle_radio = cell_radio + 1
    print('convolution...', circle_radio)
    delta_integrated = fs.convolution(field, circle_radio)
    print('done')
    return delta_integrated

def void_cleaner(void_field,rmin,bin_size,lbox):
    
    # creo el campo que voy a ir limpiando
    void_field_clean = void_field
    
    # arranca seleccionando los voids mas grandes
    vmax = np.max(void_field)
    print('max void size:', vmax)
    # tamaños de voids a explorar
    voidsizes_explored = np.arange(rmin,vmax + 1)
    print('void sizes:',voidsizes_explored)
    print('void sizes:',voidsizes_explored[::-1])
    ngrid = void_field.shape[0]
        
    # ahora me paro en los voids mas grandes y me fijo si se superponen
    # el numero del lugar me indica el radio del void.
    print(voidsizes_explored[::-1])
    
    # DONT PARALELIZE because I'm go to be writing in the share array void_Field_clean
    # and maybe there are cell that could be neccesary check multiple times and so on... 
    for void_size in voidsizes_explored[::-1]:
        
        mask = np.where(void_field_clean == void_size)
        mask_proof = np.where(void_field_clean >= rmin)
        print('candidates of size:',void_size,':',void_field_clean[mask].shape)
        print('total region candidates to void:', void_field_clean[mask_proof].shape)
        # remuevo todos los posibles centros de void que haya adentro de cada void.
        void_field_clean = remove_inner_centers(void_field, void_field_clean, mask, bin_size, ngrid, lbox, rmin)
        #print(len(mask[0]),' candidates of size', void_size)
        
        # I could pararlelize inside of this function. 
        #void_field_clean = remove_voids(void_field, void_field_clean, mask, bin_size, ngrid, lbox)
        mask = np.where(void_field_clean == void_size)
        print('survivors:', void_field_clean[mask].shape)
        mask_proof = np.where(void_field_clean >= rmin)
        print('survival region candidates to void:', void_field_clean[mask_proof].shape)
    
        
    return void_field_clean

def remove_voids(void_field, void_field_clean, mask, bin_size, ngrid, lbox):
    
    ncandidates = len(void_field[mask])
    
    # paralelize this part. 
    for i in range(ncandidates):
         
        radio = int(void_field[mask][i])
        voidsize = radio*bin_size
            
        # Ningun candidato que se superponga puede estar mas alla de 2 rvoid
        # Por lo tanto selecciono todos los centros en 2 rvoid
        binx0 = mask[0][i]
        biny0 = mask[1][i]
        binz0 = mask[2][i]
        
        posx_void = binx0*bin_size + bin_size/2
        posy_void = biny0*bin_size + bin_size/2
        posz_void = binz0*bin_size + bin_size/2
        
        void_field_clean = pg.clean_inside_sphere(binx0, biny0, binz0, radio, ngrid, void_field_clean)
        # reescribo el centro porque lo borre recien.
        void_field_clean[binx0,biny0,binz0] = radio
        
        # ahora recorro todos los centros y remuevo los que se superpongan
        bin_number = len(seleccion_bines)
        for j in range(bin_number):
            
            binx = seleccion_bines[j][0]
            biny = seleccion_bines[j][1]
            binz = seleccion_bines[j][2]
            
            # me fijo cual el tamaño del void candidato
            voidsize2 = void_field[binx,biny,binz]

            # candidate void position
            posx = binx*bin_size + bin_size/2
            posy = biny*bin_size + bin_size/2
            posz = binz*bin_size + bin_size/2
    
            dx = abs(posx-posx_void)
            dx = pg.distance_correction_periodicity(dx,lbox)
            dy = abs(posy-posy_void)
            dy = pg.distance_correction_periodicity(dy,lbox)
            dz = abs(posz-posz_void)
            dz = pg.distance_correction_periodicity(dz,lbox)
        
            # distance to void candidate position
            d = np.sqrt(dx**2 + dy**2 + dz**2)
            
            # chequeo si hay superposicion
            if (d<voidsize + voidsize2):
                void_field_clean[binx,biny,binz] = 0
            
    vmax = vmax - 1
    return void_field_clean

def clean_inside_sphere(binx, biny, binz, radio, ngrid, field):
        seleccion_bines = pg.generar_bines(binx, biny, binz, radio, ngrid)
            
        # Transponer la lista de tuplas
        transpuesta = zip(*seleccion_bines)
        # Convertir las tuplas transpuestas en arrays
        seleccion_bines = [np.array(t) for t in transpuesta]# ahora recorro todos los centros y remuevo los que se superpongan

        
        # remover todos los que esten adentro del radio
        #field[seleccion_bines] = 0  Hay que hacerlo tupla para no tener el warning ? 
        field[tuple(seleccion_bines)] = 0
        # escribo el centro de nuevo porque lo borre en el proceso anterior
        # test: void_field_clean[binx0,biny0,binz0] == 0
        return field
    
def remove_inner_centers(void_field, void_field_clean, mask, bin_size, ngrid, lbox, rmin):
    # Esta funcion va a remover para cada void en un dado tamaño todos los posibles centros 
    # que tenga en su interior
    
    ncandidates = len(void_field[mask])
    for i in range(ncandidates):
         
        radio = int(void_field_clean[mask][i])
        if radio > 0:
            voidsize = radio*bin_size
            
        # Ningun candidato que se superponga puede estar mas alla de 2 rvoid
        # Por lo tanto selecciono todos los centros en 2 rvoid
            binx0 = mask[0][i]
            biny0 = mask[1][i]
            binz0 = mask[2][i]
        
            void_field_clean = clean_inside_sphere(binx0, biny0, binz0, radio, ngrid, void_field_clean)
        # reescribo el centro porque lo borre recien.
            void_field_clean[binx0,biny0,binz0] = radio
            mask_check = np.where(void_field_clean >= rmin)
            #print('despues de remover:',radio,void_field_clean[mask_check].shape)
        
    return void_field_clean

# La función de eliminación central con numba
@njit(parallel=True)
def eliminate_central_numba(central_mask_radios_clean, bubble_ind, positions_clean, radios_clean, cpositions_ids):
    for i in prange(100): #len(cpositions_ids)):
        idx = cpositions_ids[i]
        if central_mask_radios_clean[idx] > 0:
            indices_candidatos_in = bubble_ind[idx]
            for ind in indices_candidatos_in:
                positions_clean[ind, :] = -99
                radios_clean[ind] = -99

def clean_inner_regions(field, lbox, rmin):
    bins = field.shape[0]
    bin_size = lbox / bins
    rmax = np.max(field)
    rmin = rmin / bin_size
    print('radio maximo void: ', rmax * bin_size, rmax, bin_size)
    candidates_mask = np.where(field >= rmin)

    posx = candidates_mask[0] * bin_size + bin_size / 2
    posy = candidates_mask[1] * bin_size + bin_size / 2
    posz = candidates_mask[2] * bin_size + bin_size / 2
    positions = np.array([posx, posy, posz], dtype=np.float32).T
    radios_tot = field[candidates_mask]

    voidsizes_explored = np.unique(field)
    seleccion, = np.where(voidsizes_explored >= rmin)
    voidsizes_explored = voidsizes_explored[seleccion]
    voidsizes_explored = np.sort(voidsizes_explored)
    print('voidsizes_explored:', voidsizes_explored)
    print('void sizes:', voidsizes_explored[::-1])

    start_time = time.time()
    print('creo linked list')
    # Asumiendo que tienes una función initialize_grid definida en otra parte
    grid = gsp.GriSPy(positions, N_cells=20)
    periodic = {0: (0, lbox), 1: (0, lbox), 2: (0, lbox)}
    grid.set_periodicity(periodic, inplace=True)
    grid = initialize_grid(grid)
    print('neighbors calculated in time:', time.time() - start_time, 'seg')

    positions_clean = positions.copy()
    radios_clean = radios_tot.copy()
    for radio in voidsizes_explored[::-1]:
        print('cleaning in voids of size:', radio)
        central_mask, = np.where(radios_clean == radio)
        cpositions = positions_clean[central_mask, :]
        print('candidatesss:', len(cpositions))
        if len(cpositions) == 0:
            print('no more central positions')
            continue

        indices = range(len(cpositions))
        start_time = time.time()
        print('busco vecinos en paralelo')
        results = parallel_calculate_neighbors(indices, radio, cpositions, bin_size)
        bubble_dist = [item[0] for item in results]
        bubble_ind = [item[1] for item in results]
        print('neighbors calculated')
        print('done in:', time.time() - start_time, 'seg')

        cpositions_ids = list(range(len(cpositions)))
        random.shuffle(cpositions_ids)

        start_time = time.time()
        central_mask_radios_clean = radios_clean[central_mask]
        eliminate_central_numba(central_mask_radios_clean, bubble_ind, positions_clean, radios_clean, cpositions_ids)
        print('limpieza interna done in:', time.time() - start_time, 'seg')

    del grid
    primera_limpieza, = np.where(radios_clean >= rmin)
    return positions[primera_limpieza, :], radios_clean[primera_limpieza]        
    
def calculate_neighbors(i,rvoid,positions,bin_size):
    #rvoid = radios_tot[i]*bin_size
    centre = np.zeros([1,3])
    centre[0] = positions[i,:]
    
    upper_radii = rvoid*bin_size
    lower_radii = 0.1 # *bin_size
    
    shell_dist, shell_ind = grid.shell_neighbors(
    centre,
    distance_lower_bound=lower_radii,
    distance_upper_bound=upper_radii
    )
    
    return shell_dist, shell_ind

def parallel_calculate_neighbors(indices, rvoid, positions, bin_size):
    with Pool() as pool:
        results = pool.starmap(calculate_neighbors, [(i, rvoid, positions, bin_size) for i in indices])
    return results
    
def eliminate_central(i,radios_clean,positions_clean,bubble_ind,central_mask):        
    # chequeo que el radio no sea -99 (que significa que esta borrado)
    if (radios_clean[central_mask][i]>0): 
        # todos estos candidatos hay que removerlos
        indices_candidatos_in = bubble_ind[i]
        positions_clean[indices_candidatos_in,:] = -99
        radios_clean[indices_candidatos_in] = -99
    #if ((positions_clean[central_mask][i,2] > 1435) & (positions_clean[central_mask][i,0] > 280) &
    #(positions_clean[central_mask][i,0] < 320) & (positions_clean[central_mask][i,1] > 1230) &
    #(positions_clean[central_mask][i,1] < 1250)):
    #    print('ZONA RARA', radios_clean[central_mask][i], 'pos:', positions_clean[central_mask][i,:], 'ind:',len(bubble_ind[i]))
    

def segunda_limpieza(positions, radios,lbox,rmin,bin_size):
    nvoids = len(radios)
    print('numero de voids (con superposicion):', nvoids)
    positions_clean = positions
    radios_clean = radios
    
    rmax = np.max(radios_clean)
    
    #radios_list = np.arange(rmin,rmax+1)
    radios_list = np.unique(radios)
    radios_list = np.sort(radios_list)
    
    # esto en realidad ya esta hecho.... !!
    grid = gsp.GriSPy(positions, N_cells=20)
    periodic = {0: (0, lbox), 1: (0, lbox), 2: (0, lbox)}
    grid.set_periodicity(periodic, inplace=True)
    grid = initialize_grid(grid)
    
    print(radios_list[::-1])
    for radio in radios_list[::-1]:
            
            radio_central = radio*bin_size
            
            mask, = np.where(radios_clean*bin_size == radio_central)
            print('radio:',radio*bin_size, 'voids:', len(mask))
            
            cpositions = positions_clean[mask,:]
            if (len(cpositions) == 0):
                continue
            cradios = radios_clean[mask]
            
            upper_radii = 2*radio_central
            lower_radii = 0.01#radio_central #cualquier numero menor a el tamaño de la celda
            bubble_dist, bubble_ind = grid.shell_neighbors(
            cpositions,
            distance_lower_bound=lower_radii,
            distance_upper_bound=upper_radii
            )
            k = 0
            for i in range(len(cpositions)):
                # me fijo de las distancias cuales son menor
                # que la suma de ambos radios
                #asd, = np.where(radios_clean > 0)
                #print(len(asd))
                if ((radios_clean[mask][i] > -99) & (len(bubble_dist[i])>0)):
                    
                    # le doy una diagonal de celda de changui (mi error estimado)
                    distancia_limite = radio_central + radios_clean[bubble_ind[i]]*bin_size - np.sqrt(3)*bin_size
                    #print('distancia_limite:',distancia_limite)                                         
                    sel, = np.where(bubble_dist[i] <= distancia_limite)
                    #print(len(bubble_dist[i]),sel.shape)
                    if (len(sel)>0):
                        #print('entre')
                        k = k + 1
                        indices_a_cero = bubble_ind[i][sel]
                        #positions_clean[bubble_ind[i]][sel] = 0
                        positions_clean[indices_a_cero] = -99
                        
                        #radios_clean[bubble_ind[i]][sel] = 0
                        radios_clean[indices_a_cero] = -99
                        
            print(k)
                
    final_selection = np.where(radios_clean > 0)
    return positions_clean[final_selection,:], radios_clean[final_selection]




def check_growth(mask0,mask):
    indices0 = np.zeros([len(mask0[0]),3])
    indices = np.zeros([len(mask[0]),3])

    indices[:,0] = mask[0]
    indices[:,1] = mask[1]
    indices[:,2] = mask[2]

    indices0[:,0] = mask0[0]
    indices0[:,1] = mask0[1]
    indices0[:,2] = mask0[2]
    
    set_indices = set(map(tuple, indices))

    # Filtramos indices0 para mantener solo los elementos que están en indices
    filtered_indices0 = np.array([item for item in indices0 if tuple(item) in set_indices],dtype=np.int64)
    
    if (filtered_indices0.shape[0] > 0 ):
        new_mask = (np.array(filtered_indices0[:,0]),np.array(filtered_indices0[:,1]),np.array(filtered_indices0[:,2]))
    if (filtered_indices0.shape[0] == 0):
        new_mask = -99
    
    return new_mask



