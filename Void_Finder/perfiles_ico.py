import pandas as pd
import grispy as gsp
import healpy as hp 
import programas as pg
import numpy as np
from multiprocessing import Pool
import MAS_library as MASL
from icosphere import icosphere


def calcular_perfiles(id_void):
    x = v[seleccion,0][id_void] #v[sel, 0][id_void]
    y = v[seleccion,1][id_void] #v[sel, 1][id_void]
    z = v[seleccion,2][id_void] #v[sel, 2][id_void]
    rvoid = v[seleccion,3][id_void] #v[sel, 3][id_void]
    return perfil_interpolado_icosphere(x, y, z, bineado, rvoid, field, bin_size, lbox)

def icosphere_points(x0,y0,z0,r,nu):
        
    x = []
    y = []
    z = []
    
    vertices, faces = icosphere(nu)
    x = vertices[:,0]*r
    y = vertices[:,1]*r
    z = vertices[:,2]*r
        
    return np.array(x)+x0,np.array(y)+y0,np.array(z)+z0

def perfil_interpolado_icosphere(x, y, z, bineado, rvoid, field, bin_size, lbox):
    
    ngrid = field.shape[0]
    perfil = np.zeros(bineado.shape[0] , dtype=np.float32)  # Asegúrate de que el perfil sea float32
    perfil[0] = rvoid
    
    i = 0
    for radio in bineado[1:]:
        
        radio_mpc = rvoid*radio - bin_size #resto bin_size para centrar
        
        #if radio < 4:
        #    nu = 2
        #if radio >= 4:
        #    nu = 40
        nu = 20    
        posx, posy, posz = icosphere_points(x, y, z, radio_mpc, nu)
        puntos = len(posx)
        field_count = []
        asd = 0
        for j in range(puntos):
            px = posx[j]
            py = posy[j]
            pz = posz[j]
            px = px % lbox
            py = py % lbox
            pz = pz % lbox
            
            #px = pg.correct_positions_periodicity(px, lbox)
            #py = pg.correct_positions_periodicity(py, lbox)
            #pz = pg.correct_positions_periodicity(pz, lbox)
            
            
            # px,py,pz pueden ser negativos, CORREGIR !!!
            
            pos = np.array([[px, py, pz]], dtype=np.float32)  # Asegúrate de que pos sea float32
            den = np.zeros(pos.shape[0], dtype=np.float32)
            MASL.CIC_interp(field, lbox, pos, den)
            field_count.append(den[0])
            
            
        
        perfil[1:][i] = np.mean(field_count)
        i += 1
        
    return perfil


smooth = 20
masa = '2e-03'
delta_thresold = -0.9
fbines = 540
space = 'redshift_space'

if space == 'real_space':
    name = 'pos'
    name_field = 'gal'
if space == 'redshift_space':
    name = 'posz'
    name_field = 'galz'

#field_name = 'rhodm_eul'
#field_name = 'rho'+name_field+'_'+masa+'_eul_bias_gaus_'+str(smooth)
field_name = 'gal'+name+'_RedSample_'+masa+'_'+str(smooth)   

#field = np.load('/mnt/projects/bias_mock/galaxies/smoothed_fields/PL/'+field_name+'.npy').astype(np.float32)
field = np.load('/mnt/projects/bias_mock/galaxies/smoothed_fields/PL/'+field_name+'.npy').astype(np.float32)
#field = field/np.mean(field) - 1    # activar solo para fiels hechos por raul

path_out = '/home/arodriguez/BACCO/bias-voids/Void_Finder/voids/'+space+'/'+str(fbines)+'/'
filename_out = 'perfiles_'+field_name

#field = np.load(field_name+'.npy').astype(np.float32)  # Convertir a float32
#v = np.loadtxt('/home/arodriguez/BACCO/bias-voids/Void_Finder/voids/'+space+'/'+str(fbines)+'/voids_'+masa+'_smooth_'+str(smooth)+'_clean_'+str(delta_thresold)+'.dat',skiprows=1,delimiter=',')
v = np.loadtxt('/home/arodriguez/BACCO/bias-voids/Void_Finder/voids/'+space+'/'+str(fbines)+'/'+'voids_'+field_name+'_clean_'+str(delta_thresold)+'.dat',skiprows=1,delimiter=',')
#v = np.loadtxt('/mnt/projects/bias_mock/voids/sphvds_raul-RedSample_'+masa+'_'+str(delta_thresold)+'.dat')
#v = np.loadtxt('/home/arodriguez/BACCO/bias-voids/Void_Finder/voids/'+space+'/voids_rhogalz_5e-04_eul_bias_gaus_clean_-0.9_pruebabines.dat',skiprows=1,delimiter=',')

#sel, = np.where((v[:, 0] > 300) & (v[:, 0] < 1100) & (v[:, 1] > 300) & (v[:, 1] < 1100) & (v[:, 2] > 300) & (v[:, 2] < 1100))
print('fbines:',fbines,field.shape[0])
num_procesos = 40

nbines = 30
rmin = 0
rmax = 3
bineado = np.linspace(rmin, rmax, nbines + 1)

max_rvoid = 100
min_rvoid = 0
seleccion, = np.where((v[:,3]>min_rvoid) & (v[:,3]<max_rvoid))
nvoids = len(seleccion)
print('cantidad de voids en rango:', nvoids)

rhomean = np.mean(field)
ngrid = field.shape[0]
lbox = 1440
bin_size = lbox / ngrid

if __name__ == "__main__":
    id_voids = range(nvoids)
    with Pool(processes=num_procesos) as pool:
        perfiles = pool.map(calcular_perfiles, id_voids)
    
    perfiles = np.array(perfiles)
    print('perfiles shape:',perfiles.shape)
    
    
    np.save(path_out+filename_out+'.npy',perfiles)
    
    
    #median, q1, q2 = pg.median_profile(perfiles.T, 25, 75)
    #mean = pg.mean_profile(perfiles.T)
#
    #data = {'distance': bineado[1:],
    #        'media': mean,
    #        'mediana': median,
    #        'q25': q1,
    #        'q75': q2}
#
    #df = pd.DataFrame(data)
    #df.to_csv('perfiles_interp_' + field_name + '_pruebas.csv', index=False)
