import pandas as pd
from scipy.ndimage import convolve
from scipy.signal import fftconvolve
import numpy as np 
from joblib import Parallel, delayed
from multiprocessing import Pool
import gc
import sys
import fields as fs
from concurrent.futures import ThreadPoolExecutor
from numba import njit, prange
import programas as pg 
import vfinder_mod as vf
import smoothing_library as SL
import h5py
import MAS_library as MASL
import grispy as gsp
import random
# transformo posiciones a grilla

masa = '5e-04'
fbines = 540 #the 3D field will have grid x grid x grid voxels
smooth = 0   # smoothed scale in Mpc/h
delta_threshold = -0.9
nbines = 70

# radios maximos y minimos en unidades de celdas
rmin = 6

path = '/mnt/projects/bias_mock/galaxies/'
filename = 'galposz_RedSample_'+masa
file = h5py.File(path+filename+'.hdf5','r')
pos = file['Subhalo/SubhaloPos'][()]

lbox = 1440.0 #Mpc/h ; size of box
MAS     = 'NGP'  #mass-assigment scheme
path_out = '/home/arodriguez/BACCO/bias-voids/Void_Finder/voids/redshift_space/'+str(fbines)+'/'
voidfilename_out = 'voids_'+masa+'_smooth_'+str(smooth)

bin_size = lbox/fbines

print('computing field..')
pos = pos.astype(np.float32)
# compute the field
delta = np.zeros((fbines,fbines,fbines), dtype=np.float32)
MASL.MA(pos, delta, lbox, MAS, verbose=False)
# transformo a campo de contraste densidad
delta /= np.mean(delta, dtype=np.float32);  delta -= 1.0

Filter = 'Top-Hat'
threads = 56

if (smooth == 0):
    np.save(path+'/smoothed_fields/PL/'+filename+'_smooth_'+str(smooth)+'_'+str(fbines)+'.npy', delta)
if (smooth > 0):
    print('suavizando field..')
    # compute FFT of the filter
    W_k = SL.FT_filter(lbox, smooth, fbines, Filter, threads)
    # smooth the field
    field_smoothed = SL.field_smoothing(delta, W_k, threads)
    #field_smoothed = fs.convolution(delta,smooth,bin_size)
    np.save(path+'/smoothed_fields/PL/'+filename+'_smooth_'+str(smooth)+'_'+str(fbines)+'.npy', field_smoothed)
    delta = field_smoothed
    del field_smoothed

    
print('min delta:',np.min(delta)) 

void_field = np.zeros([fbines,fbines,fbines], dtype = np.float32)


##########################################################
radios_to_convolve = pg.generar_lista(rmin,rmin+nbines)

marker_first_iteration = 1
for radio in radios_to_convolve:

    print('convolution...', radio)
    
    R = radio*bin_size
    field_integrated = fs.convolution_modified(delta, radio, bin_size)
    print('done')
    
    sel = np.where(field_integrated < delta_threshold)
    
    try:
        print('sel0 len:',len(sel0[0]))
    except NameError:
        print('sel0 no existe')
    
    if (len(sel[0]) == 0):
        print('termino busqueda de void center candidates')
        break
        
    if marker_first_iteration == 0:
        # tengo que controlar que los sel hayan estado en sel0 
        new_sel = vf.check_growth(sel0,sel)
        if (type(new_sel)==tuple):
            void_field[new_sel] = radio
            sel0 = new_sel
        if (new_sel == -99):
            print('esferas compensadas con anterioridad')
        
    if marker_first_iteration == 1:
        print('primera iteracion', 'marker first iteration:', marker_first_iteration)
        void_field[sel] = radio
        marker_first_iteration = 0
        sel0 = sel
    del field_integrated

gc.collect()

print('radios de voids encontrados:',np.unique(void_field))

    
#print('saving void candidates field')
#np.save('/home/arodriguez/BACCO/bias-voids/void_finder/'+name+'_void_field.npy',void_field)

# esto lo estoy utilizando para testo, sino comentarearlo.
#void_field = np.load('/home/arodriguez/BACCO/bias-voids/void_finder/'+name+'_void_field.npy')

print('starting cleaning..')

positions_clean, radios_clean = vf.clean_inner_regions(void_field,lbox,rmin)
#
data = {'x':positions_clean[:,0],
        'y':positions_clean[:,1],
        'z':positions_clean[:,2],
        'r':radios_clean*bin_size
        }
## Guardar los datos en un archivo CSV
df = pd.DataFrame(data)
df.to_csv(path_out+voidfilename_out+'_no_clean_'+str(delta_threshold)+'_superbin.dat',index=False)

pos, rad = vf.segunda_limpieza(positions_clean, radios_clean,lbox,rmin,bin_size)
data = {'x':pos[0][:,0],
        'y':pos[0][:,1],
        'z':pos[0][:,2],
        'r':rad*bin_size
        }
df = pd.DataFrame(data)
df.to_csv(path_out+voidfilename_out+'_clean_'+str(delta_threshold)+'_superbin.dat',index=False)

print('program finished')
