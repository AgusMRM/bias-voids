#!/bin/bash

#SBATCH --job-name=voids_field
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=50
#SBATCH --partition=batch

export OMP_NUM_THREADS=50
export MKL_NUM_THREADS=50

. /etc/profile

module purge
#conda activate
srun python /home/arodriguez/BACCO/bias-voids/perfiles_SinSmooth_paralelizado.py
