import numpy as np
import h5py
import grispy as gsp
import programas as pg

file = h5py.File('/mnt/projects/bias_mock/galpos_RedSample.hdf5','r')
pos = file['Subhalo/SubhaloPos'][()]

v0 = np.loadtxt('sphvds_raul-0.8.dat')
centres = v0[:,2:5]
nvoids = len(v0)


Lbox = 1440
grid = gsp.GriSPy(pos, N_cells=32)
periodic = {0: (0, Lbox), 1: (0, Lbox), 2: (0, Lbox)}
grid.set_periodicity(periodic, inplace=True)

bines = 20
rmin = 0.01
rmax = 5
bineado = np.logspace(np.log10(rmin),np.log10(rmax),bines) 

npart = len(pos)
rhomean = npart/Lbox**3

perfiles = np.zeros([nvoids,bines-1])
for i in range(nvoids):

    centres = v0[i,2:5]
    rvoid = v0[i,1]
    x = centres[0]
    y = centres[1]
    z = centres[2]

    perfil = pg.profile(x,y,z,rvoid,grid,bines,bineado)
    volumen = pg.profile_volume(rvoid,bines,bineado)
    
    delta = ((perfil/volumen)-rhomean)/rhomean
    perfiles[i,:] = delta
    
