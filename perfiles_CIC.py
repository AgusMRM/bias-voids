import pandas as pd
import grispy as gsp
import healpy as hp 
import programas as pg
import numpy as np
from multiprocessing import Pool
import MAS_library as MASL

def calcular_perfiles(id_void):
    x = v[sel, 2][id_void]
    y = v[sel, 3][id_void]
    z = v[sel, 4][id_void]
    rvoid = v[sel, 1][id_void]
    return perfil_interpolado(x, y, z, bineado, rvoid, field, bin_size, lbox)

def numero_de_puntos(distance):
    nmin = 10
    if distance < 1:
        p = nmin
    if distance >= 1:    
        p = int(nmin * distance ** 2)
    return p

def perfil_interpolado(x, y, z, bineado, rvoid, field, bin_size, lbox):
    ngrid = field.shape[0]
    perfil = np.zeros(bineado.shape[0] - 1, dtype=np.float32)  # Asegúrate de que el perfil sea float32

    i = 0
    for radio in bineado[1:]:
        puntos = numero_de_puntos(radio)
        posx, posy, posz = pg.fibo_points(x, y, z, radio * rvoid, puntos, ngrid)
        
        field_count = []
        for j in range(puntos):
            px = posx[j]
            py = posy[j]
            pz = posz[j]
            
            pos = np.array([[px, py, pz]], dtype=np.float32)  # Asegúrate de que pos sea float32
            den = np.zeros(pos.shape[0], dtype=np.float32)
            MASL.CIC_interp(field, lbox, pos, den)
            field_count.append(den[0])
            
        perfil[i] = np.mean(field_count)
        i += 1
        
    return perfil

field_name = 'rhogal_2e-04_eul_bias_expansion'
field = np.load('/mnt/projects/bias_mock/galaxies/'+field_name+'.npy').astype(np.float32)
field = field/np.mean(field) - 1
#field_name = 'density_field_mio'
#field = np.load(field_name+'.npy').astype(np.float32)  # Convertir a float32
v = np.loadtxt('/mnt/projects/bias_mock/voids/sphvds_raul-RedSample_2e-04-0.8.dat')

sel, = np.where((v[:, 2] > 300) & (v[:, 2] < 1100) & (v[:, 3] > 300) & (v[:, 3] < 1100) & (v[:, 4] > 300) & (v[:, 4] < 1100))

num_procesos = 30
nvoids = 500  # len(v)
nbines = 30 
rmin = 0
rmax = 3
bineado = np.linspace(rmin, rmax, nbines + 1)

rhomean = np.mean(field)
ngrid = field.shape[0]
lbox = 1440
bin_size = lbox / ngrid

if __name__ == "__main__":
    id_voids = range(nvoids)
    with Pool(processes=num_procesos) as pool:
        perfiles = pool.map(calcular_perfiles, id_voids)
    
    perfiles = np.array(perfiles)
    print('perfiles shape:',perfiles.shape)
    
    
    np.save('/home/arodriguez/BACCO/bias-voids/perfiles/perfiles_'+field_name+'_NEW-INTERPOLATION_.npy',perfiles)
    
    
    median, q1, q2 = pg.median_profile(perfiles.T, 25, 75)
    mean = pg.mean_profile(perfiles.T)

    data = {'distance': bineado[1:],
            'media': mean,
            'mediana': median,
            'q25': q1,
            'q75': q2}

    df = pd.DataFrame(data)
    df.to_csv('perfiles_interp_' + field_name + '_pruebas.csv', index=False)
