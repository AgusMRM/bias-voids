import pandas as pd
import grispy as gsp
import healpy as hp 
import programas as pg
import numpy as np
from multiprocessing import Pool
import MAS_library as MASL
from icosphere import icosphere


def calcular_perfiles(id_void):
    x = v[seleccion,0][id_void] #v[sel, 0][id_void]
    y = v[seleccion,1][id_void] #v[sel, 1][id_void]
    z = v[seleccion,2][id_void] #v[sel, 2][id_void]
    rvoid = v[seleccion,3][id_void] #v[sel, 3][id_void]
    return perfil_interpolado_icosphere(x, y, z, bineado, rvoid, field, bin_size, lbox)

def icosphere_points(x0,y0,z0,r,nu):
        
    x = []
    y = []
    z = []
    
    vertices, faces = icosphere(nu)
    x = vertices[:,0]*r
    y = vertices[:,1]*r
    z = vertices[:,2]*r
        
    return np.array(x)+x0,np.array(y)+y0,np.array(z)+z0

def perfil_interpolado_icosphere(x, y, z, bineado, rvoid, field, bin_size, lbox):
    
    ngrid = field.shape[0]
    perfil = np.zeros(bineado.shape[0] , dtype=np.float32)  # Asegúrate de que el perfil sea float32
    perfil[0] = rvoid
    
    i = 0
    for radio in bineado[1:]:
        
        radio_mpc = rvoid*radio - bin_size #resto bin_size para centrar
        
        #if radio < 4:
        #    nu = 2
        #if radio >= 4:
        #    nu = 40
        nu = 20    
        posx, posy, posz = icosphere_points(x, y, z, radio_mpc, nu)
        puntos = len(posx)
        field_count = []
        asd = 0
        for j in range(puntos):
            px = posx[j]
            py = posy[j]
            pz = posz[j]
            px = px % lbox
            py = py % lbox
            pz = pz % lbox
            
            #px = pg.correct_positions_periodicity(px, lbox)
            #py = pg.correct_positions_periodicity(py, lbox)
            #pz = pg.correct_positions_periodicity(pz, lbox)
            
            
            # px,py,pz pueden ser negativos, CORREGIR !!!
            
            pos = np.array([[px, py, pz]], dtype=np.float32)  # Asegúrate de que pos sea float32
            den = np.zeros(pos.shape[0], dtype=np.float32)
            MASL.CIC_interp(field, lbox, pos, den)
            field_count.append(den[0])
            
            
        
        perfil[1:][i] = np.mean(field_count)
        i += 1
        
    return perfil


smooth = 0
masa = '5e-04'
delta_thresold = -0.9
fbines = 540
space = 'real_space'

if space == 'real_space':
    name = 'pos'
    name_field = 'gal'
if space == 'redshift_space':
    name = 'posz'
    name_field = 'galz'

num_procesos = 112

nbines = 30
rmin = 0
rmax = 3
bineado = np.linspace(rmin, rmax, nbines + 1)



v = np.loadtxt('/home/arodriguez/BACCO/bias-voids/IATE/voids-Pylians/PyLvoids_6_rmin_16.dat',skiprows=1,delimiter=',')
max_rvoid = 25
min_rvoid = 20
seleccion, = np.where((v[:,3]>=min_rvoid) & (v[:,3]<max_rvoid))
nvoids = len(seleccion)
print('cantidad de voids en rango:', nvoids)


#pathperfiles = '/home/arodriguez/BACCO/bias-voids/IATE/perfiles/4f/'
pathperfiles = '/home/arodriguez/BACCO/bias-voids/IATE/voids-Pylians/'
pathfields   = '/home/arodriguez/BACCO/bias-voids/IATE/fields_smooth/k0.75_4f/'
# Define los nombres de los campos en una lista
fields = [#'F0_0', 'F1_0', 'F2z_0', 'F3z_0','F4z_0', 'simulationz_smooth_0']
          #'F0_3', 'F1_3', 'F2z_3', 'F3z_3','F4z_3', 'simulationz_smooth_3']
          'F0_6', 'F1_6', 'F2_6', 'F3_6','F4_6', 'simulation_smooth_6']
          #'F0_9', 'F1z_9', 'F2z_9', 'F3z_9','F4z_9', 'simulationz_smooth_9']
          #'F0_12', 'F1z_12', 'F2z_12', 'F3z_12','F4z_12', 'simulationz_smooth_12']
          #'F0_15', 'F1z_15', 'F2z_15', 'F3z_15','F4z_15', 'simulationz_smooth_15']  
          #'F0_18', 'F1z_18', 'F2z_18', 'F3z_18','F4z_18', 'simulationz_smooth_18']  

            
for field_name in fields:
    # Carga el campo correspondiente
    field = np.load(pathfields+field_name+'.npy').astype(np.float32)
    print(f'Procesando campo: {field_name}')
    
    # Procesa el campo como en tu código original
    print('Cantidad de voids en rango:', nvoids)
    
    rhomean = np.mean(field)
    ngrid = field.shape[0]
    lbox = 1440
    bin_size = lbox / ngrid
    
    # Ejecuta el cálculo de perfiles en paralelo
    id_voids = range(nvoids)
    with Pool(processes=num_procesos) as pool:
        perfiles = pool.map(calcular_perfiles, id_voids)
    
    perfiles = np.array(perfiles)
    print('Shape de perfiles:', perfiles.shape)
    
    # Guarda los perfiles con un nombre específico para cada campo
    #output_filename = f'/home/arodriguez/BACCO/bias-voids/pruebas_fit/perfiles/perfiles_{field_name.split(".")[0]}_rmin={min_rvoid}_k0.25.npy'
    output_filename = pathperfiles + 'perfiles_'+field_name+'_radio_'+str(min_rvoid)+'_'+str(max_rvoid)+'bines_'+str(nbines)+'.npy'
    np.save(output_filename, perfiles)
    print(f'Archivo guardado: {output_filename}')
