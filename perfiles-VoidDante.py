import pandas as pd
import grispy as gsp
import healpy as hp 
import programas as pg
import numpy as np
from joblib import Parallel, delayed

def calcular_perfiles(id_void):
    x = v[id_void, 2]
    y = v[id_void, 3]
    z = v[id_void, 4]
    rvoid = v[id_void, 1]
    prof, particles = pg.profile_bias(x,y,z,rvoid,field,nbines,bineado,lbox)
    return prof/particles 


    
masa = '2e-04'
delta_threshold = -0.8
#field_name = 'rhogal_'+masa+'_eul_bias_gaus'
field_name = 'rhodm_eul'
field = np.load('/mnt/projects/bias_mock/galaxies/'+field_name+'.npy')
#field_name = 'galpos_RedSample_'+masa+'_smoothed_2'
#field_name = 'galpos_RedSample_'+masa+'_Top-Hat_rscale_5'
#field = np.load('/mnt/projects/bias_mock/galaxies/smoothed_fields/'+field_name+'.npy')

#field_name = 'rhodm_eul_smoothed_2'
#field_name = 'rhogal_'+masa+'_eul_bias_expansion_smoothed_2'
#field = np.load('/mnt/projects/bias_mock/galaxies/smoothed_fields/'+field_name+'.npy')

v = np.loadtxt('/mnt/projects/bias_mock/voids/sphvds_raul-RedSample_'+masa+str(delta_threshold)+'.dat')
#v = np.loadtxt('/home/arodriguez/BACCO/bias-voids/void_finder/voids_'+masa+'_1_clean_'+str(delta_threshold)+'.dat',skiprows=1,delimiter=',')
num_procesos = 25
nvoids = len(v)
nbines = 30 
rmin = 0
rmax = 4
bineado = np.linspace(rmin,rmax,nbines+1)
vmin = 0
vmax = 100
vsizes = v[:,1]
filename_out='/home/arodriguez/BACCO/bias-voids/perfiles/perfiles_prop_'+field_name+'-'+str(vmin)+'-'+str(vmax)+'-'+masa+'_'+str(delta_threshold)+'_vdante.csv'

rhomean = np.mean(field)
ngrid = field.shape[0]
lbox = 1440
bin_size = lbox/ngrid

vseleccion, = np.where((vsizes>vmin) & (vsizes<vmax))
id_voids = vseleccion
print('void seleccionados: ', len(id_voids))

if __name__ == "__main__":
      # Puedes ajustar este número según tu hardware
    
    #id_voids = range(nvoids)
    #perfiles = Parallel(n_jobs=num_procesos)(delayed(calcular_perfiles)(id_void) for id_void in id_voids)
    perfiles = Parallel(n_jobs=num_procesos)(delayed(calcular_perfiles)(i) for i in id_voids)
    
    perfiles = np.array(perfiles, dtype=np.float32)
    
perfiles = (perfiles -rhomean)/rhomean
np.save('/home/arodriguez/BACCO/bias-voids/perfiles/perfiles_'+field_name+'_'+str(vmin)+'-'+str(vmax)+'-'+masa+str(delta_threshold)+'_vdante.npy',perfiles)

median,q1,q2 = pg.median_profile(perfiles.T,25,75)
mean = pg.mean_profile(perfiles.T)

data = {'distance': bineado[1:],
        'media': mean,
        'mediana': median,
        'q25': q1,
        'q75': q2}

df = pd.DataFrame(data)
df.to_csv(filename_out,index=False)
