import pandas as pd
import grispy as gsp
import healpy as hp 
import programas as pg
import numpy as np
import MAS_library as MASL
from icosphere import icosphere
from multiprocessing import Pool, Array
import ctypes

np.seterr(invalid='ignore')

def init_shared_array(shared_array_base, shape):
    global shared_array
    shared_array = np.frombuffer(shared_array_base, dtype=np.float32)
    shared_array = shared_array.reshape(shape)

def P2(mu):
    polinomio = (3*(mu**2) -1)/2
    return polinomio

def calcular_perfiles(id_void, v, seleccion, bineado_r, bineado_mu, bin_size, lbox):
    x = v[seleccion, 0][id_void]
    y = v[seleccion, 1][id_void]
    z = v[seleccion, 2][id_void]
    rvoid = v[seleccion, 3][id_void]
    field_local = shared_array.reshape(field_shape)
    return correlation(x, y, z, bineado_r, bineado_mu, rvoid, field_local, bin_size, lbox)

def icosphere_points(x0,y0,z0,r,nu):
        
    x = []
    y = []
    z = []
    
    vertices, faces = icosphere(nu)
    x = vertices[:,0]*r
    y = vertices[:,1]*r
    z = vertices[:,2]*r
        
    return np.array(x)+x0,np.array(y)+y0,np.array(z)+z0

def pi_coord(pz,z,lbox):
    dz = abs(pz - z)
    if dz > lbox/2:
        dz = -(lbox - dz)
    if (pz-z)<0:
        dz = -dz
    return dz

def correlation(x, y, z, bineado_s, bineado_mu, rvoid, field, bin_size, lbox):
    
    ngrid = field.shape[0]
    bines_s = bineado_s.shape[0] - 1
    bines_mu = bineado_mu.shape[0] - 1
    perfil_smu = np.zeros([bines_s,bines_mu], dtype=np.float32)  # Asegúrate de que el perfil sea float32
    counts_smu = np.zeros([bines_s,bines_mu], dtype=np.float32)
    abin_s = bineado_s[1] - bineado_s[0]
    abin_mu = bineado_mu[1] - bineado_mu[0]
    
    # abin_pp es paralela-perpendicular (sigma/pi) que es igual a s. 
    abin_pp = abin_s
    perfil_pp = np.zeros([bines_s,bines_s],dtype = np.float32)
    counts_pp = np.zeros([bines_s,bines_s],dtype = np.float32)
    
    
    bin_mu_centro = int(bines_mu/2)
    for radio in bineado_s[1:]:
        
        radio_mpc = rvoid*radio - bin_size/2 #resto bin_size para centrar
        
        nu = 20    
        posx, posy, posz = icosphere_points(x, y, z, radio_mpc, nu)
        puntos = len(posx)
        #field_count = []
        
        for j in range(puntos):
            px = posx[j]
            py = posy[j]
            pz = posz[j]
            
            # calculo distancias para sigma-pi
            dx = abs(px - x)
            if dx > lbox/2:
                dx = lbox - dx
            dy = abs(py - y)
            if dy > lbox/2:
                dy = lbox - dy
            dz = abs(pz - z)
            if dz > lbox/2:
                dz = lbox - dz
            
            pi = pi_coord(pz,z,lbox)
            sigma = np.sqrt(dx**2 + dy**2)
            s = np.sqrt(pi**2 + sigma**2)
            mu = pi/s
            
            ### sigo con los puntos para la esfera            
            px = px % lbox
            py = py % lbox
            pz = pz % lbox        
        
            pos = np.array([[px, py, pz]], dtype=np.float32)  # Asegúrate de que pos sea float32
            den = np.zeros(pos.shape[0], dtype=np.float32)
            MASL.CIC_interp(field, lbox, pos, den)
            
            bin_s = int((s/rvoid)/abin_s) #+ bin_sigma_centro
            bin_mu = int((mu/abin_mu)) + bin_mu_centro
            if bin_mu == 60:
                bin_mu = 0
            if bin_mu > 60:
                print('PROBLEMAAAAA:',bin_mu, mu, pi)
            
            perfil_smu[bin_s,bin_mu] += den[0]
            counts_smu[bin_s,bin_mu] += 1
            
            bin_sigma = int((sigma/rvoid)/abin_pp)
            bin_pi    = int((abs(pi)/rvoid)/abin_pp)
            
            perfil_pp[bin_sigma,bin_pi] += den[0]
            counts_pp[bin_sigma,bin_pi] += 1
    
    return perfil_smu/counts_smu, perfil_pp/counts_pp

smooth = 0
masa = '2e-03'
delta_thresold = -0.9
fbines = 540
space = 'redshift_space'

if space == 'real_space':
    name = 'pos'
    name_field = 'gal'
if space == 'redshift_space':
    name = 'posz'
    name_field = 'galz'

#field_name = 'rhodm_eul'
field_name = 'rho'+name_field+'_'+masa+'_eul_bias_gaus_'+str(smooth)
#field_name = 'gal'+name+'_RedSample_'+masa+'_'+str(smooth)   

#field = np.load('/mnt/projects/bias_mock/galaxies/smoothed_fields/PL/'+field_name+'.npy').astype(np.float32)
field = np.load('/mnt/projects/bias_mock/galaxies/smoothed_fields/PL/'+field_name+'.npy').astype(np.float32)
#field = field/np.mean(field) - 1    # activar solo para fiels hechos por raul

path_out = '/home/arodriguez/BACCO/bias-voids/voids/'+space+'/'+str(fbines)+'/correlations/'
filename_out_smu = 'xi_smu_' + field_name
filename_out_pp  = 'xi_pp_'  + field_name


void_name = 'voids_'+field_name+'_clean_'+str(delta_thresold)+'.dat'
#v = np.loadtxt('/home/arodriguez/BACCO/bias-voids/voids/'+space+'/'+str(fbines)+'/voids/'+'voids_'+field_name+'_clean_'+str(delta_thresold)+'.dat',skiprows=1,delimiter=',')

#void_name = 'voids_'+'gal'+name+'_RedSample_'+masa+'_'+str(smooth)+'_clean_'+str(delta_thresold)+'.dat'
v = np.loadtxt('/home/arodriguez/BACCO/bias-voids/voids/'+space+'/'+str(fbines)+'/voids/'+void_name,skiprows=1,delimiter=',')

print('analizando:',field_name, void_name)

#sel, = np.where((v[:, 0] > 300) & (v[:, 0] < 1100) & (v[:, 1] > 300) & (v[:, 1] < 1100) & (v[:, 2] > 300) & (v[:, 2] < 1100))
print('fbines:',fbines,field.shape[0])
num_procesos = 112

nbines_r = 30
nbines_mu = 60

rmin = 0
rmax = 4
mumin = -1
mumax = 1

bineado_r = np.linspace(rmin, rmax, nbines_r + 1)
bineado_mu = np.linspace(mumin,mumax,nbines_mu + 1)
print(bineado_r)
print(bineado_mu)

max_rvoid = 100
min_rvoid = 0
seleccion, = np.where((v[:,3]>min_rvoid) & (v[:,3]<max_rvoid))
nvoids = len(seleccion)
print('cantidad de voids en rango:', nvoids)

ngrid = field.shape[0]
lbox = 1440
bin_size = lbox / ngrid


#if __name__ == "__main__":
#    id_voids = range(nvoids)
#    with Pool(processes=num_procesos) as pool:
#        perfiles_smu, perfiles_pp = pool.map(calcular_perfiles, id_voids)
#    
#    perfiles_smu = np.array(perfiles_smu, dtype=np.float32)
#    perfiles_pp  = np.array(perfiles_smu, dtype=np.float32)
#
#    print('perfiles shape:',perfiles.shape)
    
    

if __name__ == "__main__":
    shared_array_base = Array(ctypes.c_float, field.flatten(), lock=False)
    field_shape = field.shape
    
    id_voids = range(nvoids)
    with Pool(processes=num_procesos, initializer=init_shared_array, initargs=(shared_array_base, field_shape)) as pool:
        perfiles = pool.starmap(
            calcular_perfiles, 
            [(id_void, v, seleccion, bineado_r, bineado_mu, bin_size, lbox) for id_void in id_voids]
        )
        perfiles_smu, perfiles_pp = zip(*perfiles)
        
    np.save(path_out + filename_out_smu + '.npy', perfiles_smu)
    np.save(path_out + filename_out_pp + '.npy', perfiles_pp)