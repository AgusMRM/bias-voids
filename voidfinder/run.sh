#!/bin/bash

#SBATCH --job-name=voidfinder
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=56
#SBATCH --time 2-0:00
#SBATCH --partition=batch

export OMP_NUM_THREADS=56
export MKL_NUM_THREADS=56
export PYTHONUNBUFFERED=1

. /etc/profile

module purge
srun python /home/arodriguez/BACCO/bias-voids/voidfinder/voidfinder.py
#srun python /home/arodriguez/BACCO/bias-voids/voidfinder/perfiles_ico.py
