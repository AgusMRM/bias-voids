import pandas as pd
import programas as pg
from scipy.ndimage import convolve
from scipy.signal import fftconvolve
import numpy as np 
from joblib import Parallel, delayed
import gc
import sys

name = 'rhogal_eul_bias_expansion'
path = '/mnt/projects/bias_mock/'
field = np.load(path+name+'.npy')
nbines = 20
fbines = field.shape[0]
nthreads = nbines
rhomean = np.mean(field)

delta_threshold = -0.9



def convolucion_campos(field,circle_radio):
    
    i = circle_radio
    print(i)
    campo_extendido = np.pad(field,  [(i, i)]*3, mode='wrap')
    print('campo extendido ',i)
    kernel_radio = i + 1
    kernel = pg.generate_circle_matrix_3d(540, kernel_radio, dim=3)
    # normalize the kernel
    kernel = kernel/np.sum(kernel)

    # Realizar la convolución
    campo_convolucionado = fftconvolve(campo_extendido, kernel, mode='same')
    
    center_field = campo_convolucionado[i:i+fbines,i:i+fbines,i:i+fbines]
    # guardar campo
    file = np.save(path+'convolution_'+name+str(i)+'.npy',center_field)
    
    

#Parallel(n_jobs=nthreads, require='sharedmem')(delayed(convolucion_campos)(field,i) for i in np.arange(nbines))                                                                             

for i in range(nbines):
    funcion = convolucion_campos(field,i)
    gc.collect() 

    

field = np.load(path+'convolution_'+name+str(0)+'.npy')
delta = (field-rhomean)/rhomean
mask, = np.where(delta < delta_threshold)
ncandidates = len(mask)
array_decreciente = np.array(list(range(nbines, 0, -1)))

for k in array_decreciente:
    
    #selecciono todos los que son void en la escala maxima
    field_number = array_decreciente[k]
    field_integrated = np.load(path+'convolution_'name+str(field_number)+'.npy')

    for i in range(ncandidates)
        
        # selecciono cada centro de candidato a void
        center = field[mask[i]]
                
        # me fijo la densidad en el radio maximo
        density_max = field_integrated[mask[i]]
        if density_max < density_thresold:
             # es void
    
    