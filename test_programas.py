import programas a pg
import numpy as np
import pytest

def test_interpolador1():
    
    field = np.zeros([3,3,3])
    # en z = 0 hago valores distintos a cero
    field[0,0,0] = 1
    field[1,0,0] = 0.5
    field[2,0,0] = 0.5

    field[0,1,0] = 0.5
    field[0,2,0] = 1

    field[1,1,0] = 0.3
    field[1,2,0] = 0.2

    field[2,1,0] = 1
    field[2,2,0] = 0.6

    bin_size = 1
    x = 1.2; y = 1.6; z = 0
        
    interpolacion = interpolador(x,y,z,field,bin_size)
    print(interpolacion-0.334)
    assert abs(interpolacion - 0.334) < 0.01
    
def test_interpolador1():
    
    field = np.zeros([3,3,3])
    # en z = 0 hago valores distintos a cero
    field[0,0,0] = 1
    field[1,0,0] = 0.1
    field[2,0,0] = 0.4

    field[0,1,0] = 0.5
    field[0,2,0] = 1

    field[1,1,0] = 0.3
    field[1,2,0] = 0.2

    field[2,1,0] = 1
    field[2,2,0] = 0.6

    bin_size = 1
    x = 2.7; y = 1.8; z = 0
        
    interpolacion = interpolador(x,y,z,field,bin_size)
    assert abs(interpolacion - 0.334) < 0.011
    
    interpolacion = interpolador(2.7,1.8,0,field,bin_size)
    assert abs(interpolacion - 0.824) < 0.011