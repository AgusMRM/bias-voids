#!/bin/bash

#SBATCH --job-name=voidfinder
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=55
#SBATCH --partition=small

export OMP_NUM_THREADS=55
export MKL_NUM_THREADS=55

. /etc/profile

module purge
##srun python /home/arodriguez/BACCO/bias-voids/perfiles-VoidDante.py
##srun python /home/arodriguez/BACCO/bias-voids/perfiles_SinSmooth_paralelizado.py
srun python /home/arodriguez/BACCO/bias-voids/perfiles_CIC_chatgpt.py
##srun python /home/arodriguez/BACCO/bias-voids/void_finder/perfiles_SinSmooth_paralelizado-SmoothedFields.py
