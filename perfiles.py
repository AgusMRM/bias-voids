# programa para hacer los perfiles de los voids. 
# EDITAR LO SIGUIENTE:
# - mascara void_selecion para seleccionar los voids
# - nombre del file

import numpy as np 
import scipy.stats as sp
import h5py 
import programas as pg
import warnings
import pandas as pd
import MAS_library as MASL

warnings.filterwarnings("ignore")

path = '/mnt/projects/bias_mock/'
filename_bias = 'rhogal_eul_bias_gaus'   #sin extension de formato !! 

v = np.loadtxt('sphvds_raul-0.8.dat')
void_selection, = np.where(v[:,1]>23)
nvoids = len(v[void_selection])


centres = v[void_selection,2:5]
field = np.load(path+filename_bias+'.npy')

lbox = 1440
bines = 10
rmin = 0
rmax = 3
#bineado = np.logspace(np.log10(rmin),np.log10(rmax),bines)
bineado = np.linspace(rmin,rmax,bines)

profile = np.zeros([bines,nvoids])

for i in range(nvoids):
    x = v[void_selection][i,2]
    y = v[void_selection][i,3]
    z = v[void_selection][i,4]
    rvoid = v[void_selection][i,1]
    perfil, particulas = pg.profile_bias(x,y,z,rvoid,field,bines,bineado,lbox)
    profile[:,i] = perfil/particulas

    
    
perfil_medio = pg.mean_profile(profile)
perfil_median,q1,q2 = pg.median_profile(profile,25,75)


data = {'distance': bineado,
        'media': perfil_medio,
        'mediana': perfil_median,
        'q25': q1,
        'q75': q2}

df = pd.DataFrame(data)
df.to_csv('perfiles_'+filename_bias+'_grandes.dat',index=False)

    
    