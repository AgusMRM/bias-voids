import pandas as pd
from scipy.signal import fftconvolve, oaconvolve
import numpy as np 
from joblib import Parallel, delayed
import gc
import sys
import programas as pg

def open_fields(nfields,path,name):
    # fijarse si los archivos existen y si no existen
    # llamar a la funcion convolucion.py que los crea.  
    fields = []
    for i in range(nfields):
        field = np.load(path+'convolution_'+name+str(i)+'.npy')
        fields.append(field)
        
    return fields

def convolution(field,circle_radio,bin_size):
    
    i = circle_radio
    fbines = field.shape[0]
    campo_extendido = np.pad(field,  [(i, i)]*3, mode='wrap')
    
    #kernel_radio = i + 1
    #kernel = pg.generate_circle_matrix_3d(fbines, kernel_radio, dim=3)
    
    kernel = pg.weight_sphere(fbines, bin_size, circle_radio, 1000)
    # normalize the kernel
    kernel = kernel/np.sum(kernel)
    
    # Realizar la convolución
    campo_convolucionado = fftconvolve(campo_extendido, kernel, mode='same')
    center_field = campo_convolucionado[i:i+fbines,i:i+fbines,i:i+fbines]
    
    return center_field

def convolution_modified(field,circle_radio,bin_size):
    # esta modificada para poder hacer convoluciones que sean fraccion de bin
    i = int(circle_radio+1)
    fbines = field.shape[0]
    campo_extendido = np.pad(field,  [(i, i)]*3, mode='wrap')
    
    #kernel_radio = i + 1
    #kernel = pg.generate_circle_matrix_3d(fbines, kernel_radio, dim=3)
    
    kernel = pg.weight_sphere(fbines, bin_size, circle_radio, 5000)
    # normalize the kernel
    kernel = kernel/np.sum(kernel)
    
    # Realizar la convolución
    campo_convolucionado = fftconvolve(campo_extendido, kernel, mode='same')
    center_field = campo_convolucionado[i:i+fbines,i:i+fbines,i:i+fbines]
    
    return center_field

def convolution_modified(field,circle_radio,bin_size):
    # esta modificada para poder hacer convoluciones que sean fraccion de bin
    i = int(circle_radio+1)
    fbines = field.shape[0]
    campo_extendido = np.pad(field,  [(i, i)]*3, mode='wrap')
    
    #kernel_radio = i + 1
    #kernel = pg.generate_circle_matrix_3d(fbines, kernel_radio, dim=3)
    
    kernel = pg.weight_sphere(fbines, bin_size, circle_radio, 5000)
    # normalize the kernel
    kernel = kernel/np.sum(kernel)
    
    # Realizar la convolución
    campo_convolucionado = fftconvolve(campo_extendido, kernel, mode='same')
    center_field = campo_convolucionado[i:i+fbines,i:i+fbines,i:i+fbines]
    
    return center_field
